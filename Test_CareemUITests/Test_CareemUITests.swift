//
//  Test_CareemUITests.swift
//  Test_CareemUITests
//
//  Created by Jorge Flor on 30/04/17.
//  Copyright © 2017 Jorge Flor. All rights reserved.
//

import XCTest

class Test_CareemUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_1_SearchBatMan() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.segmentedControls.children(matching: .button).matching(identifier: "Title").element(boundBy: 0).tap()
        
        let searchField = tablesQuery.children(matching: .searchField).element
        searchField.typeText("Batman")
        app.buttons["Search"].tap()
        
        sleep(5)
        
        /// Verify if have this movie
        XCTAssert(XCUIApplication().tables.staticTexts["Batman Returns"].exists)
        
        
    }
    
    func test_2_SearchNoInfo() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.segmentedControls.children(matching: .button).matching(identifier: "Title").element(boundBy: 0).tap()
        
        let searchField = tablesQuery.children(matching: .searchField).element
        searchField.typeText(" ")
        app.buttons["Search"].tap()
        
        sleep(5)
        
        /// Verify if have alert no data
        XCTAssert(XCUIApplication().sheets["No Data"].exists)
        
        
        
    }
    
    func test_3_SearchSuggestions() {
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
        
        let app = XCUIApplication()
        let tablesQuery = app.tables
        tablesQuery.segmentedControls.children(matching: .button).matching(identifier: "Title").element(boundBy: 0).tap()
        
        let searchField = tablesQuery.children(matching: .searchField).element
        searchField.typeText(" ")
        
        sleep(120)
        
        /// Verify if have alert no data
//        XCTAssert(XCUIApplication().sheets["No Data"].exists)
        
        
        
    }
    
    
    
    
}
