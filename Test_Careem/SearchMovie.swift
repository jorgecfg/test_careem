//
//  ViewController.swift
//  Test_Careem
//
//  Created by Jorge Flor on 30/04/17.
//  Copyright © 2017 Jorge Flor. All rights reserved.
//

import UIKit
import Alamofire
import AFNetworking
import TagListView


/// Enum of Variables Used on UserDefault
enum enumDefaults:String {
    case SearchHistory = "Search_History"
}

class SearchMovie: UIViewController,UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, TagListViewDelegate {

    //MARK:- Variables
    @IBOutlet weak var sbMovies: UISearchBar!
    @IBOutlet weak var tbMovies: UITableView!
    var MovieList:NSMutableArray!
    var refreshControl:UIRefreshControl!
    var uDefaults = UserDefaults.standard
    var vwSuggestions:UIView!
    let screenSize = UIScreen.main.bounds
    
    
    
    //MARK:- View
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        /// Init Vars
        self.MovieList = NSMutableArray()
        
        /// Add pull to refresh
        self.refreshControl = UIRefreshControl()
        self.refreshControl.tintColor = UIColor.gray
        self.refreshControl.addTarget(self, action: #selector(self.refreshData(sender:)), for: .valueChanged)
        self.tbMovies.addSubview(self.refreshControl) // Add on tableview
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //MARK:- SearchBar
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        /// Execute LoadData
        loadData()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        /// Remove Suggestions
        closeSuggestions()
        
        /// Close keyboard
        view.endEditing(true)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        /// Verify if have more than 1 history to show suggestions
        if LoadHistory().count > 0 {

            /// Show suggestions
            showSuggestions()
            
        }
        
    }
    
    //MARK:- Refresh Control
    func refreshData(sender: UIRefreshControl) {
        /// Verify if have something to search
        if (self.sbMovies.text?.characters.count)! > 0{
            
            /// Load Data
            loadData()

        }
        /// Remove refreshcontrol
        self.refreshControl.endRefreshing()

    }
    
    
    //MARK:- Load Data
    func loadData()  {
        
        /// Close Keyboard
        view.endEditing(true)
        
        /// Close Suggestions
        closeSuggestions()
        
        /// Clear Actual List
        MovieList.removeAllObjects()
        
        /// Variable of search
        /// TRIM remove white space and newlines
        let movieSearch:String = sbMovies.text!.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
        
        /// Url Final for search
        let urlFinal:String = "\(urlBaseMovie)\(movieSearch)"
        
        /// Call the service, and return the results
        Alamofire.request(urlFinal).responseJSON { response in
            
            
            do {
                let json = try JSONSerialization.jsonObject(with: response.data!, options: .allowFragments)
                
                /// Verify if return Data
                if((json as AnyObject).count > 0){
                    
                    /// Data returned
                    let Dados:NSDictionary = json as! NSDictionary

                    /// Log Dados
                    print("Dados: \(Dados)")
                    
                    
                    /// Verify if the return is for Results or Errors
                    if (Dados.object(forKey: "errors") != nil){
                        /// Show message alert
                        let alert = UIAlertController(title: "Ops...", message: "Houston, we have a problem... Try again... ", preferredStyle: .actionSheet)
                        alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                            /// Focus insearch
                            self.sbMovies.becomeFirstResponder()
                        }))
                        self.present(alert, animated: true, completion: nil)
                        
                    }
                    else{
                        /// If no have error...
                    
                        /// Get the node 'results'
                        let Results:NSArray = Dados.object(forKey: "results") as! NSArray
                        
                        /// Verify if have results
                        if Results.count > 0 {

                            /// Save the search onhistory
                            self.SaveHistory(Therm: self.sbMovies.text!)

                            
                            // Save Local
                            self.MovieList = NSMutableArray(array: Results as! [Any], copyItems: true)

                            /// Verify if have Error
                            
                            /// Reload DataTable
                            self.tbMovies.reloadData()

                        }
                        else {

                            /// Show Message
                            let alert = UIAlertController(title: "No Results", message: "We don't find any movie containing '\(self.sbMovies.text!)'", preferredStyle: .actionSheet)
                            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: { (action) in
                                /// Focus insearch
                                self.sbMovies.becomeFirstResponder()
                            }))
                            self.present(alert, animated: true, completion: nil)

                        
                        
                        }
                        
                    }
                    
                }
                else{
                    /// NO DATA
                    /// Present message
                }
                
                
            } catch {
                /// Ops... something not good happen
            }
            
        }
        
        
        
    }
    
    
    //MARK:-TableView
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.MovieList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //Cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseMovieCell", for: indexPath) as! tableviewCellMovie

        /// Entry from Row
        let LineEntry:NSDictionary = (self.MovieList.object(at: indexPath.row) as? NSDictionary)!
        
        /// Load Data in model
        let MovieM = MovieModel.init(data:LineEntry)
        
        //Show Values
        cell.lbMovieName.text = MovieM.nmMovie
        cell.lbMovieRelease.text = MovieM.dtReleaseMovie
        cell.lbMovieOverview.text = MovieM.descMovie
        cell.imMoviePoster.setImageWith(MovieM.urlPicMovie! as URL)
        
        //returning cell
        return cell

    }

    
    //MARK:- Suggestions
    func showSuggestions(){
        
        /// View Master of suggestions
        vwSuggestions = UIView(frame: CGRect(x: 0, y: 108, width: self.screenSize.width, height: self.screenSize.height - 108))
        vwSuggestions.backgroundColor = UIColor(red: 0.9, green: 0.9, blue: 0.9, alpha: 1.0)
        

        /// Component of tags
        let Tags:TagListView = TagListView(frame: CGRect(x: 10, y: 10, width: self.screenSize.width - 20, height: self.screenSize.height - 20))
        Tags.delegate = self // Apply Delegate
        vwSuggestions.addSubview(Tags) // Add to Subview Master of Suggestions
        
        /// Change Font and Size
        Tags.textFont = UIFont.systemFont(ofSize: 25)
        
        /// Change tag Color
        Tags.tagBackgroundColor = UIColor.orange
        
        
        /// Limit
        let LimitOfSuggestions = 10
        
        /// Control of Limit
        var CountSuggestions = 0
        
        /// Loop for History
        for HistItem in LoadHistory() {
        
            /// Verify if limit was not reached
            if CountSuggestions < LimitOfSuggestions {

                /// Item of History
                let Item:String = HistItem as! String
                
                /// Add Tags
                Tags.addTag(Item)
                
            }
            
            /// Increment Count
            CountSuggestions += 1
            
        }
        
        /// Add view of suggestions in view
        view.addSubview(vwSuggestions)
    }
    
    
    func closeSuggestions() {
        /// Remove from Superview
        vwSuggestions.removeFromSuperview()
    }
    
    /// Suggestion Pressed
    func tagPressed(_ title: String, tagView: TagView, sender: TagListView) {
        
        /// Input the Text to searchBar
        sbMovies.text = title
        
        /// Make Search
        loadData()
    }
    
    
    //MARK:- History
    func LoadHistory() -> NSMutableArray {

        /// Init Return
        var oReturn:NSMutableArray = NSMutableArray()
        
        /// Verify if is Nil
        if uDefaults.object(forKey: enumDefaults.SearchHistory.rawValue) != nil{
            
            let ArrayHistory:NSArray = uDefaults.object(forKey: enumDefaults.SearchHistory.rawValue) as! NSArray
            
            /// Return of History from UserDefaults
            oReturn = NSMutableArray(array: ArrayHistory as! [Any], copyItems: true)

        }
        
        
        
        /// Return variable
        return oReturn
        
    }
    
    func SaveHistory(Therm:String) {
        
        /// Load History
        let History:NSMutableArray = NSMutableArray(array: LoadHistory() as! [Any], copyItems: true)
        
        
        /// Add Therm on history
        History.insert(Therm, at: 0)
        
        
        /// Save on UserDefaults
        uDefaults.setValue(History, forKeyPath: enumDefaults.SearchHistory.rawValue)
        
    }
    

    
}

