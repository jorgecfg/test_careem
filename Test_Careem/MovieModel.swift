//
//  MovieModel.swift
//  Test_Careem
//
//  Created by Jorge Flor on 30/04/17.
//  Copyright © 2017 Jorge Flor. All rights reserved.
//

import UIKit

class MovieModel {
    var nmMovie:        String?
    var dtReleaseMovie: String?
    var urlPicMovie:    NSURL?
    var descMovie:      String?
    
    /// Init obj with NSDictionary of DataResult
    init(data:NSDictionary?) {

        /*
         EXAMPLE RETURN
         
         Item: {
         adult = 0;
         "backdrop_path" = "/xyWSYXUuLrtnDWjyY6ONh502Bz9.jpg";
         "genre_ids" =     (
         28,
         12,
         18
         );
         id = 414906;
         "original_language" = en;
         "original_title" = "The Batman";
         overview = "Batman makes his first stand-alone appearance in the DC Extended Universe.";
         popularity = "1.195961";
         "poster_path" = "/3zJABNuAjAccFdAMQ3D5RJF0v93.jpg";
         "release_date" = "2019-12-31";
         title = "The Batman";
         video = 0;
         "vote_average" = 0;
         "vote_count" = 2;
         }
        */
        
        
        
        
        /// Fetch Variables
        self.nmMovie = data?.object(forKey: "title") as? String
        self.descMovie = data?.object(forKey: "overview") as? String
        self.dtReleaseMovie = data?.object(forKey: "release_date") as? String
        
        /// Some entries no have image for poster_path
        /// For this case, use a URL of default
        if !((data?.object(forKey: "poster_path")) is NSNull) {
            /// Get URL of Pic
            let tempURLPic:String = (data?.object(forKey: "poster_path") as? String)!
            
            /// Add URL Inicial
            let UrlDefault:String = "http://image.tmdb.org/t/p/w92"
            
            /// Convert to URL and save to self.urlPicMovie
            let urlComplete = "\(UrlDefault)\(tempURLPic)"
            self.urlPicMovie = NSURL(string: urlComplete)
            
        }
        else {
            /// URL Of image default
            self.urlPicMovie = NSURL(string: "https://firebasestorage.googleapis.com/v0/b/hashtag-a72a4.appspot.com/o/placeHolder%403x.png?alt=media&token=18359845-1593-457b-ab17-295f8a3e3dea")
        }
        
    }
}
