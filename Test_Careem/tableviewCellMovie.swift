//
//  movieCell.swift
//  Test_Careem
//
//  Created by Jorge Flor on 30/04/17.
//  Copyright © 2017 Jorge Flor. All rights reserved.
//

import UIKit

class tableviewCellMovie: UITableViewCell {
    
    //Fields
    @IBOutlet weak var lbMovieName: UILabel!
    @IBOutlet weak var lbMovieRelease: UILabel!
    @IBOutlet weak var imMoviePoster: UIImageView!
    @IBOutlet weak var lbMovieOverview: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        /// Init Code
        
        
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        
    }
    
}
